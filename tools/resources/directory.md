# What does it do?

The directory (or people) API allows you to search the duke public directory, and access information about students and staff. This includes names, LDAP keys, and netids. It searches the duke LDAP system and allows you to find records of specific people.

## /ldap/people

This node takes the parameter q, which stands for query. It searches the directory for people whose names or netids contain q, and returns the result as an array of "person" objects, as shown below. The ldapkey is a unique identifier given to every person at Duke (faculty, staff, students), and can be used for looking people up in further API calls. The fields sn and givenName are the person's Last and First names respectively. The netid is pretty self explanatory. display_name is just a convenient string to use when referring to the person looked up. url is the url for more detail on that person.

```javascript
[
  {
    "ldapkey": "f8231696-11d1-11b2-af99-9442cd793b76",
    "sn": "Meisterburger",
    "givenName": "Burgermeister",
    "netid": "bam999",
    "display_name": "Burgermeister Meisterburger",
    "url": "https://streamer.oit.duke.edu/ldap/people/f8231696-11d1-11b2-af99-9442cd793b76"
  }
]
```

## /ldap/people/{ldapkey}

This call is made by appending a person's ldap key (as shown above) to the end of the request path. It returns a single person object, with a few extra fields. The nickname field is just a shorthand name for the person. The emails array contains email addresses for the person in string form. post_office_boxcontains an array of strings, each of which correspond to a line in that person's address. primary_affiliation can have values of "Student", "Faculty", or "Affiliate". Finally, phones is an array containing the phone numbers of the person in string form.

```javascript
{
  "ldapkey": "f1831696-1dd1-11b2-af99-9442cd793b76",
  "sn": "Jenny",
  "givenName": "Jenny",
  "netid": "jjj1",
  "display_name": "Jenny Jenny",
  "nickname": "Jenny",
  "primary_affiliation": "Student",
  "emails": [
    "numberonthewall@duke.edu"
  ],
  "post_office_box": [
    "Box 8675309",
    "Durham, NC 27708"
  ],
  "phones": [
    "+1 919 867 5309"
  ],
  "url": "https://streamer.oit.duke.edu/ldap/people/f1831696-1dd1-11b2-af99-9442cd793b76"
}
```

## /ldap/people/netid/{netid}

This API call identifies a single person using their netid, which you append to the end of the path you request. It returns the exact same object as /ldap/people/{ldapkey}, except inside an array for some reason.

# How do I use it?

As with all of the duke APIs, all you have to do is send an HTTP GET request to (aka pull down the webpage of) the url listed, appending "?access_token=YOURTOKEN", as well as "&PARAMETER=VALUE" as necessary. This is explained in greater detail in the Using APIs tutorial. For this particular API, I'd suggest letting your users search using the q parameter in /ldap/people, and letting them pick from a list of netids. Then pull down details from /ldap/people/netid/{netid}. If you want a way to make people verify their own netids (sign in using duke credentials), check out Shibboleth. Asking for netids and passwords on your own is really really really really discouraged.


# Related Resources:
[Shibboleth Tools](../Shibboleth.md)