3rd Party APIs
==============

An API is, in general, a method of allowing your application to communicate with another application. This could allow you to send or receive data, or issue commands on behalf of your users. For more information about what an API is, check its page on the wiki.

The APIs below are selected third-party APIs. That is, we here in Co-Lab can't promise anything about them, but we find them helpful and use them in our own work. They each are very easy to work with, and feature a brief description of what the API is capable of doing, along with a link to each's official documentation. I hope you find these useful!

Be sure to read the documentation carefully. Many of these services have special rules and limitations as to how often you can query the API.

## 1. Google Maps API

[Documentation Page](https://developers.google.com/maps/)

The Google Maps API describes methods for very quickly and easily putting a map on your webpage or mobile app. It's very easy to get started with, but has many powerful features, enabling you to customize the maps appearance, draw on it, and display different map layers.

## 2. Transloc API

[Documentation Page](http://api.transloc.com/doc/)

Have an app that plans a duke student's entire day? Consider using the Transloc API to make sure that they make it to the bus on time. This API can list and track buses and routes at Duke and around Durham, and provide your users with accurate arrival time estimates for each stop.

## 3. Facebook Graph API

[Documentation Page](https://developers.facebook.com/docs/javascript/quickstart/v2.2)

Facebook is a web of interconnections between people. This API allows you to parse through, and attempt to make sense of, these connections. It also allows you to execute actions on a user's facebook account (assuming they allow you to!). They also have a list of useful Software Development Kits to help you.

## Related Resources: 
[API - wiki](http://dev.colab.duke.edu/wiki/api)

[Duke Public APIs](http://dev.colab.duke.edu/resources/Duke-Public-APIs)