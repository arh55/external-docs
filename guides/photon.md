# How to connect a photon to Duke's wifi

This guide will help you connect a [photon](https://store.particle.io/collections/photon) to DukeOpen.

---------------

1. Install Particle CLI: https://github.com/spark/particle-cli
2. Connect your photon using the USB
3. In your terminal, run `particle serial mac` to get the photon’s mac address (must be in green listening mode)
4. Register your photon at https://dukereg.duke.edu/. (You will need to login with your netid).
5. In your terminal, run `particle setup wifi`. Let it autoscan for networks. It will probably register each available network more than once, so just scroll down the list.
6. Choose ‘DukeOpen’
7. Let it automatically detect security settings.

You should be good to go after it finishes that. You’ll know it’s connected when it is breathing blue.