# How to use the Duke API (to look someone up using their netid)

This tutorial requires you to know:
 
 - [Basic HTML](languages/html/intro.md)
 - [Basic Javascript](languages/javascript/intro.md) (or you can translate it to any language that can make https requests)
 - [A basic understanding of HTTP is helpful](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Example_session)

---------------

The Duke APIs are a powerhouse of the Co-Lab. They allow you to extract information from Duke itself, and use that to make your applications smarter. In some cases you can even modify things using Duke APIs. This tutorial will show you the most basic case.

## 0: Before starting...

We're going to register a new application with the [App Manager](appmanager.colab.duke.edu). It tracks all of the 'applications' using the Duke API, so that anyone creating malicious or dangerous applications can be contacted. Sign in with your netid, and fill in all the info as you might see fit. Our application is just going to be a mini-directory.

## 1: Go to the [APIDocs](apidocs.colab.duke.edu) page and test your app id

Just to make sure that it works, and to figure out the endpoint we're going to use (https://api.colab.duke.edu/identity/v1/{netid}), go to the APIDocs page and paste your new app id into it. Try making a request using your own netid and see that it works.

## 2: Write a little html file with a script tag in it

Below is the one we're using. It contains a form, whose submission we will handle, as well as a script tag that we will place our api-calling script into.

```html
<!DOCTYPE html>
<html>
<head>
    <title>my directory search</title>
</head>
<body>
    <form action="" method="NONE">
        <input type="text" paceholder="Enter a netid..." name="netid">
        <input type="submit">
    </form>
</body>
<script>
    //OUR SCRIPT WILL GO HERE
</script>
</html>
```

## 3: Write the script to call the API, get an identity, and write it on the page

Below is the code to actually make the api call and slap it on the page. Each line is commented to explain what it's doing. As a whole, it's not too bad!

```javascript
//attach the `getIdentity` function to the `submit` event of the first form on the page 
document.forms[0].addEventListener('submit', getIdentity);

//the getIdentity function will expect a submission Event as its only parameter
function getIdentity(submissionEvent) {
    //prevent the form submission from doing its default thing (sending the user to a new page)
    submissionEvent.preventDefault();
    //pull the value from the 'netid' textbox
    var netid = submissionEvent.target.netid.value;
    //create a new request
    var req = new XMLHttpRequest();
    //'open' the request, setting the http method and the target url
    req.open('GET','https://api.colab.duke.edu/identity/v1/' + netid);
    //add the x-api-key header (with our app's client id as the value) to authenticate this request
    req.setRequestHeader('x-api-key','MYCLIENTID');
    //create a function to be executed when the 'load' event occurs (when we get a response)
    req.onload = function(response) {
        //you can also console.log(response) to take a look at all of the information about the response
        var responseData = response.target.responseText;
        //parse the text into an object
        var id = JSON.parse(responseData);
        //replace our page with the result
        document.body.innerHTML = 'Hello, ' + id.displayName;
    }
    //send the request!
    req.send();
    //another way of preventing the form submission from doing its default thing is to return false on the event handler. It basically tells the browser "This handler took care of everything. Ignore any other event handlers"
    return false;
}
```