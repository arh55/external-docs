# How to get yourself a Server \(for free!\)

This guide requires you to know:

* [How to use ssh to log into a server](/guides/ssh.md)

---

## 0: What is VM Manage

VM-Manage is a pilot service providing students and instructors with semester-long access to linux virtual machines \(VMs\) and Linux Docker containers for software development projects. Typical uses of this service are to develop and test servers for Innovation Co-Lab projects or coursework. Think of VM-Manage as an alternative to running a server in your dorm room - you have complete control of the server, and you are responsible for server security and backing up your work.

VM-Manage VMs are well-suited to acting as backend servers for mobile applications or public-facing web services because they have **public IP** addresses and are available 24x7. However, since this is a software development sandbox, **you should not run production or high traffic servers on VM-Manage machines**.

## 1: Getting a server

Go to [https:\/\/vm-manage.oit.duke.edu](https://vm-manage.oit.duke.edu) and login with your netID and password, and click "New" in the tab at upper right corner.

![](/assets/vm-1.png)

The form should be pre-filled with your information. Fill in necessary fields and click "Next"

![](/assets/vm-2.png)

Here, fill in information about your project.

![](/assets/vm-3.png)

Choose the server image you'd like to use. The recommended choice is Debian Jessie. Debian Jessie is a pure base image and you may use to configure or install services needed.

![](/assets/vm-4.png)

Next is the Agreement Page. You'll need to agree to the terms.

![](/assets/vm-5.png)

In the "Finish" page, you'll see a summary of your request. Make sure your information is filled in correctly, and click "Request VM"

You'll be brought to the "Manage" tab, and the VM is displayed. The ***Full Name*** is the domain name for your server, and the ***Initial Password*** is your password for logging into the server using ssh.

## 2: Requesting an Alias

You're allowed an *.colab.duke.edu alias for each of your VM. To request an alias, go to the "Register Alias" tab at the upper right corner. In the page, click "Register a new alias/CNAME for a server".

![](/assets/vm-6.png)

Fill in the alias you prefer, and the matching server name (the ***Full Name*** previously mentioned). Then click "Request Alias". When the alias is successfully registered, you'll receive a notification email, and you may use the alias to visit or ssh into the server.

