# Intro to Front-End JavaScript *Language Guide*

__Language Info__

| Property  |     Value     |
|-----------|:-------------:|
| Scripting       |         Yes       |
| Dynamic         |    Yes            |
| Typing          | Dynamic           |
| Inheretance     | Prototypical      |
| Object Oriented | Classes Supported in ES6/ES2015 |
| How is it run?  | Interpreted (JIT) |


This tutorial requires you to know:
 
 - Nothing! (but you should have Chrome installed)

---------------

[JavaScript](https://en.wikipedia.org/wiki/JavaScript) was originally put together by NetScape as a way of adding [client-side](../../whatis/architecture-properties/client-side.md) functionality into webpages. Over the years it was standardized as 'ECMAScript' (for some copyright reason or something). Browsers historically have had a hard time keeping to this standard (much to the dismay of developers. It's one of the reasons developers frequently hate on IE), but recently have done a much _much_ __much__ better job. As of writing, Edge, Chrome, Firefox, and Safari all follow the ES6 (also called ES2015) standard with >90% of tests passing. This will make our lives as engineers so much better.

Tools you'll always want to keep near when programming JS (for the web):

 - [ES6 Compatability Table](https://kangax.github.io/compat-table/es6/): keeps you up to date on how far browsers are in complying with the ECMAScript Standard
 - [CanIUse](http://caniuse.com/): A wonderful resource for knowing what web apis, html tags, and css properties are available for you to use in browsers. 
 - [Mozilla Documentation](https://developer.mozilla.org/en-US/): A great reference for all web technologies. Alternatively search 'mdn _____' and you'll get info on whatever you need.