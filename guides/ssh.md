# How to use SSH to log into a machine

This guide requires you to know:
 
 - [How to use Bash (the terminal)](languages/bash/intro.md)
 - [How to Allocate a Server](vm-manage.md)

---------------

ssh allows you to connect and use a terminal remotely on network-connected machines. Here, we'll look at some of the most common use cases of ssh, and how easy it is!

## 0: Before starting...

OSX and Linux users have ssh pre-installed on their machines, but windows users should probably install [git bash](https://git-scm.com/downloads) or another bash terminal. You can also use ssh session managers like MobaXTerm or PuTTY, but it's probably better to understand the underlying command first, before jumping into those.

Additionally, you should have some remote machine ready to connect to! The Co-Lab VMs are usually good for undersanding how to use ssh. Note: Duke VMs all start with the default username 'bitnami'.

## 1: Connecting to a server

If you've got a remote machine, whose IP or hostname you know (as well as a username to log in with), the format of the most basic ssh login is below:

```bash
ssh username@hostname
```

You'll then get a warning saying your computer doesn't recognize the signature of the remote machine (because it's never seen it before!). You can go ahead and say 'yes', and your local machine will save the fingerprint. If you try to connect again and the fingerprint is different, you'll get a big warning, because that's a sign that someone might be listening in on your connection.
After you say yes to 'continue connecting?', you'll see a password prompt for your user. As you type, you won't see any letters popping up. This is expected. If you enter the password correctly, you'll see you've now opened a terminal on the remote machine, starting in your remote user's home folder. Easy peasy, right? 

![ssh login gif](ssh/1.gif)

## 2: Using scp for copying files between your machine and the remote one 

scp is useful but easy! You can use it to copy files from your machine, to your server, and back again. The syntax is very similar to ssh.

```
scp origin destination
```

Where origin and destination are in the format
```
username@hostname:/path/to/file 
```

If you omit the `username@hostname:` portion, it assumes you mean your local system. This way, you can simply do

```bash
scp ~/myText.txt toolbox@myServer.com:~
```

to copy 'myText.txt' from my home folder locally to my home folder on myServer.com.

![scp example gif](ssh/2.gif)

## 3: Using a private key to log in

If you get a server from Amazon's AWS, or prefer a greater level of security than a password, you can use rsa keys for security. Check the glossary for a _very_ brief understanding of how rsa works. The public key goes on the server you want to connect to, and you use your private key to log in. Since only the private key can make messages that are decryptable by the public key, the server knows only the owner of that key could be talking to it. 

The ssh command for connecting using a private key is simple:

```bash
ssh username@hostname -i /path/to/key.key
```   

![ssh using a key gif](ssh/3.gif)

Now, you may not even be prompted for a password! Amazon installs a public key by default and gives you a private key, but you can also do this yourself on the Co-Lab VMs!

__Beware: You can lock yourself out of your server permanently like this. Your private key is like a real key, so if you lose it, you can't get in, we can't get in, no one can get in. There are no RSA locksmiths (we hope)! On the other hand, if you make a bunch of duplicates, and someone swipes one, they'll be able to walk through the front door. Just be careful with your keys and always be sure to make backups!__

Run `ssh-keygen` locally (which you can skip if it says you already have a keypair), using the default save location. Then, run `scp ~/.ssh/id_rsa.pub username@hostname:~/.ssh/authorized_keys` to copy your public key to the remote server as an authorized key. 
If you don't want to be prompted for a password on each login in addition to your key, you edit /etc/ssh/sshd_config  on your server (you'll have to use sudo). Replace the line that starts with 'PermitRootLogin' with 'PermitRootLogin without-password', and at the end of the file, add 'PasswordAuthentication no'. This allows login without using a password, and then turns off the password prompt. A server restart will put this into effect. Again, beware that your password will no longer allow login to your server.

