# Language Guides

These guides will give you some experience working with different programming languages. Each of them will explain some of the context around a language, and walk you through building a few programs that illustrate their features. Every language has its strengths and weaknesses, and part of being an engineer is knowing what tool is right for the job. Here are the guides we have (so far!) organized by where they are typically used: 

### Web Languages

#### [Client-Side](../whatis/architecture/client-server.md)

 - HTML
 - CSS
 - JavaScript
 
#### [Server-Side](../whatis/architecture/client-server.md)

 - JavaScript (using Node.js)
 - Python (using Django)
 - Java (using.. something)

### Desktop Languages

 - C
 - C++
 - Java

### Embedded/internet of things/hardware (arduino/photon/etc) Languages

 - C on the Arduino/Photon
 - What to do with my raspberry pi???
 - z80 assembly
 - x86 assembly

### Scripting Languages

 - Bash (a.k.a. 'The Terminal')
 - Python
 - JavaScript
 
 TODO