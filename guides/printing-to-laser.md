Using Adobe Illustrator to print to the laser cutter
======

## File Setup
* Log into the computer on the **Windows** side, *not* PC side
	* NOTE: If you are designing something in Illustrator on your own computer to print later, it doesn't matter whether you're using Mac of PC, but we only have a PC driver to print to the Epilog lasers
* Open Adobe Illustrator, create a **24” x 12”** artboard
* Drag in images you’d like to etch/engrave or vector files you’d like to cut out
* Make sure path lines for vectors you want cut out are exactly **.001 pts** with no object color infill
* Lines and images above .001 pts will be etched in grayscale

## Printer Setup
* Make sure the green light for the exhaust fan is on
* Put your material flat on the print bed
* On the Epilog Zing printer, press the `>Focus` button to bring out the focus beam and drop down the spring
* Use the `Up` and `Down` buttons on the printer to bring the print bed up and down to focus the laser on your material. The drop-down spring should just barely touch the top of your material
* Click `Reset` to return the laser to print-ready position

## Printing
* Navigate to `File > Print` (ctrl + P)
* Change Printer: to `Epilog Zing`
* Click the `Setup...` button in the bottom left, (`continue`)
* Click the `Preferences` button to bring up the blue Epilog Zing preferences menu
* Change the Piece Size dimensions to **24” x 12”**
* Use the handy Epilog Zing Material Settings sheet, our notebook, or the Epilog laser speeds and feeds wiki to look up recommended raster/vector settings for the material you’re using
* Click: `Okay > Print > Done` to save those print settings and return to your artboard
* Navigate to `File > Print` (ctrl + P) again and make sure the print preview looks like the correct image and dimensions
* Now click the `Print` button. Your file will be sent to the printer
* On the Epilog Zing printer, press the green `Go` button